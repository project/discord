<?php

namespace Drupal\discord\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\discord\Form
 *
 * @ingroup discord
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'discord_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['discord.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('discord.settings');

    $form['discord_webhook_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook URL'),
      '#description' => $this->t('Enter your <a href="https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks" target="_blank">Webhook URL</a> from Integrations in Discord server settings. Discord webhook looks like https://discord.com/api/webhooks/000000000000000000/XYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXYZXY'),
      '#default_value' => $config->get('discord_webhook_url'),
      '#required' => TRUE,
    ];
    $form['discord_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Override the username setup in Discord?'),
      '#default_value' => $config->get('discord_username'),
    ];
    $form['discord_avatar_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image URL'),
      '#default_value' => $config->get('discord_avatar_url'),
      '#description' => $this->t('What image would you use for your Discord Bot?'),
    ];
    if (empty($config->get('discord_webhook_url'))) {
      $this->messenger()->addWarning($this->t('Discord sending message page will be available after you fill "<strong>Webhook URL</strong>" field'));
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('discord.settings');
    $config
      ->set('discord_webhook_url', trim($form_state->getValue('discord_webhook_url')))
      ->set('discord_username', $form_state->getValue('discord_username'))
      ->set('discord_avatar_url', $form_state->getValue('discord_avatar_url'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
