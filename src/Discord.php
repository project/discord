<?php

namespace Drupal\discord;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Send messages to Discord.
 */
class Discord {

  use StringTranslationTrait;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $config;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private $httpClient;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a Discord object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Module configuration.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   HTTP Client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger.
   */
  public function __construct(ConfigFactoryInterface $config, ClientInterface $http_client, LoggerChannelFactoryInterface $logger) {
    $this->config = $config;
    $this->httpClient = $http_client;
    $this->logger = $logger;

  }

  /**
   * Send message to the Discord.
   *
   * @param string $message
   *   The message sent to the channel.
   * @param string $username
   *   The username displayed in the channel.
   * @param string $avatar
   *   The avatar URL.
   *
   * @return bool|object
   *   Discord response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendMessage($message, $username = '', $avatar = '') {
    $config = $this->config->get('discord.settings');
    $webhook_url = $config->get('discord_webhook_url');

    if (empty($webhook_url)) {
      $this->messenger->addError($this->t('You need to enter a webhook!'));
      return FALSE;
    }

    $this->logger->get('discord')
      ->info('Sending message "@message" as "@username"', [
        '@message' => $message,
        '@username' => $username,
      ]);

    $config = $this->prepareMessage($webhook_url, $username, $avatar);
    $result = $this->sendRequest(
      $config['webhook_url'], $message, $config['message_options']
    );

    return $result;
  }

  /**
   * Sends embedded message to the Discord.
   *
   * @param array $data
   *   Data to be sent as embedded message to the channel.
   * @param string $username
   *   The username displayed in the channel.
   * @param string $avatar
   *   The avatar URL.
   *
   * @return bool|object
   *   Discord response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function sendEmbed(array $data, $username = '', $avatar = '') {
    $config = $this->config->get('discord.settings');
    $webhook_url = $config->get('discord_webhook_url');

    if (empty($webhook_url)) {
      $this->messenger->addError($this->t('You need to enter a webhook!'));
      return FALSE;
    }

    $config = $this->prepareMessage($webhook_url, $username, $avatar);

    return $this->sendEmbeddedRequest(
      $config['webhook_url'], $data, $config['message_options']
    );
  }

  /**
   * Prepare message meta fields for Discord.
   *
   * @param string $webhook_url
   *   Webhook for Discord basic functions.
   * @param string $username
   *   The bot name displayed in the channel.
   *
   * @return array
   *   Config array.
   */
  protected function prepareMessage($webhook_url, $username, $avatar) {
    $config = $this->config->get('discord.settings');
    $message_options = [];

    if (!empty($username)) {
      $message_options['username'] = $username;
    }
    elseif (!empty($config->get('discord_username'))) {
      $message_options['username'] = $config->get('discord_username');
    }

    if(!empty($avatar)) {
      $message_options['avatar_url'] = $avatar;
    }
    elseif (!empty($config->get('discord_avatar_url'))) {
      $message_options['avatar_url'] = $config->get('discord_avatar_url');
    }

    return [
      'webhook_url' => $webhook_url,
      'message_options' => $message_options,
    ];
  }

  /**
   * Send message to the Discord with more options.
   *
   * @param string $webhook_url
   *   Webhook for Discord basic functions.
   * @param string $message
   *   The message sent to the channel.
   * @param array $message_options
   *   An associative array, it can contain:
   *     - username: The bot name displayed in the channel;
   *     - avatar_url: The bot icon displayed in the channel;
   *
   * @return \Psr\Http\Message\ResponseInterface|bool
   *   Can contain:
   *   TO DO: update data with Discord values
   *                          success    fail         fail
   *     - data:                ok       No hooks     Invalid channel specified
   *     - status message:      OK       Not found    Server Error
   *     - code:                200      404          500
   *     - error:               -        Not found    Server Error
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function sendRequest($webhook_url, $message, array $message_options = []) {
    $headers = [
      'Content-Type' => 'application/json',
    ];
    $message_options['content'] = $this->processMessage($message);
    $sending_data = json_encode($message_options);
    // $sending_data = 'payload=' . urlencode(json_encode($message_options));
    $logger = $this->logger->get('discord');
    $logger->info($sending_data);

    try {
      $response = $this->httpClient->request('POST', $webhook_url, ['headers' => $headers, 'body' => $sending_data]);
      // $response = $this->httpClient->request('POST', $webhook_url, ['headers' => $headers, 'body' => $sending_data]);
      $logger->info('Message was successfully sent!');
      return $response;
    }
    catch (ServerException $e) {
      $logger->error('Server error! It may appear if you try to use unexisting chatroom.');
      watchdog_exception('discord', $e);
      return FALSE;
    }
    catch (RequestException $e) {
      $logger->error('Request error! It may appear if you entered the invalid Webhook value.');
      watchdog_exception('discord', $e);
      return FALSE;
    }
  }

  /**
   * Sends embedded message to the Discord with more options.
   *
   * @param string $webhook_url
   *   Webhook for Discord basic functions.
   * @param array $data
   *   Data sent as embedded message to the channel.
   * @param array $message_options
   *   An associative array, it can contain:
   *     - username: The bot name displayed in the channel.
   *     - avatar_url: The bot icon displayed in the channel.
   *
   * @return \Psr\Http\Message\ResponseInterface|bool
   *   Can contain:
   *   TO DO: update data with Discord values
   *                          success    fail         fail
   *     - data:                ok       No hooks     Invalid channel specified
   *     - status message:      OK       Not found    Server Error
   *     - code:                200      404          500
   *     - error:               -        Not found    Server Error
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function sendEmbeddedRequest($webhook_url, array $data, array $message_options = []) {
    $headers = [
      'Content-Type' => 'application/json',
    ];

    $data['description'] = $this->processMessage($data['description']);

    // Cleanup and limit size.
    $data['description'] = strip_tags($data['description']);
    $data['description'] = substr($data['description'], 0, 1000);
    // Remove hashtag from color field and convert to decimal.
    $data['color'] = hexdec(str_replace('#', '', $data['color']));
    // If color is not set (default), remove it.
    if ($data['color'] === 0) {
      unset($data['color']);
    }
    // Remove empty URL address.
    if ($data['url'] === '') {
      unset($data['url']);
    }
    // Remove author, if name is empty.
    if ($data['author']['name'] === '') {
      unset($data['author']);
    }

    $message_options['embeds'][] = $data;
    $sending_data = json_encode($message_options);

    try {
      return $this->httpClient->request('POST', $webhook_url, [
        'headers' => $headers,
        'body' => $sending_data,
      ]);
    }
    catch (ServerException $e) {
      $logger = $this->logger->get('discord');
      $logger->error('Server error! It may appear if you try to use unexisting chatroom.');
      watchdog_exception('discord', $e);

      return FALSE;
    }
    catch (RequestException $e) {
      $logger = $this->logger->get('discord');
      $logger->error('Request error! It may appear if you entered the invalid Webhook value.');
      watchdog_exception('discord', $e);

      return FALSE;
    }
  }

  /**
   * Replaces links with discord friendly tags. Strips all other html.
   *
   * @param string $message
   *   The message sent to the channel.
   *
   * @return string
   *   Replaces links with discord friendly tags. Strips all other html.
   */
  protected function processMessage($message) {
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";

    if (preg_match_all("/$regexp/siU", $message, $matches, PREG_SET_ORDER)) {
      $i = 1;
      $links = [];
      foreach ($matches as $match) {
        $new_link = "<$match[2] | $match[3]>";
        $links['link-' . $i] = $new_link;
        $message = str_replace($match[0], 'link-' . $i, $message);
        $i++;
      }
      $message = strip_tags($message);
      foreach ($links as $id => $link) {
        $message = str_replace($id, $link, $message);
      }
    }
    return $message;
  }

}
