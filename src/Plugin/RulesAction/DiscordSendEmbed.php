<?php

namespace Drupal\discord\Plugin\RulesAction;

use Drupal\discord\Discord;
use Drupal\rules\Core\RulesActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'Discord send embedded message' action.
 *
 * @RulesAction(
 *   id = "rules_discord_send_embed_message",
 *   label = @Translation("Send embedded message to Discord"),
 *   category = @Translation("Discord"),
 *   context_definitions = {
 *     "title" = @ContextDefinition("string",
 *       label = @Translation("Title"),
 *       description = @Translation("Title for the message which will be sent to Discord (ie. node.title.value)."),
 *     ),
 *     "description" = @ContextDefinition("string",
 *       label = @Translation("Content"),
 *       description = @Translation("Content of the message which will be sent to Discord. You can use markdown to format the message (ie direct input {{ node.body.value | striptags }})."),
 *     ),
 *     "url" = @ContextDefinition("string",
 *       label = @Translation("URL"),
 *       description = @Translation("URL for the message which will be sent to Discord (ie node.path.alias)."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *     "author" = @ContextDefinition("string",
 *       label = @Translation("Author"),
 *       description = @Translation("Author name for the message (ie node.uid.entity.name.value)."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *     "color" = @ContextDefinition("string",
 *       label = @Translation("Color"),
 *       description = @Translation("Color, that will be shown next to message (ie. #00ff00)."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *     "username" = @ContextDefinition("string",
 *       label = @Translation("Username"),
 *       description = @Translation("Override the default username."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *     "avatar_url" = @ContextDefinition("string",
 *       label = @Translation("Avatar URL"),
 *       description = @Translation("Set a full path to an image to override the default avatar."),
 *       default_value = NULL,
 *       required = FALSE,
 *     ),
 *   }
 * )
 */
class DiscordSendEmbed extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * Discord service.
   *
   * @var \Drupal\discord\Discord
   */
  protected $discordService;

  /**
   * Constructs a DiscordSendMessage object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\discord\Discord $discord_service
   *   The Discord manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Discord $discord_service) {
    $this->discordService = $discord_service;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var \Drupal\discord\Discord $discord_service */
    $discord_service = $container->get('discord.discord_service');

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $discord_service
    );
  }

  /**
   * Sends embedded message to discord.
   *
   * @param string $title
   *   Title for the embed.
   * @param string $description
   *   Content of the embed.
   * @param string $url
   *   URL address for link from title.
   * @param string $author
   *   Author name for the embed.
   * @param string $color
   *   Color for embed strip on the left side.
   * @param string $username
   *   The Discord username.
   * @param string $avatar_url
   *   The Discord avatar URL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function doExecute($title, $description, $url = '', $author = '', $color = '', $username = '', $avatar_url = '') {
    $data = [
      'title' => $title,
      'description' => $description,
      'url' => $url,
      'author' => ['name' => $author],
      'color' => $color,
    ];

    $this->discordService->sendEmbed($data, $username, $avatar_url);
  }

}
